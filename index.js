// oddEvenChecker

function oddEvenChecker(number){
	if (typeof number === "number" ) {
		if (number % 2 == 0) {
			console.log("The number is even.");
		} else if(number % 2 == 1) {	
			console.log("The number is odd.");
		}
	} else {
		console.log("Invalid Input.")
	};
};

oddEvenChecker(124869);

// budgetChecker
function budgetChecker(amount){
	if (typeof amount === "number") {
		if (amount > 4000) {
			console.log("You are over the budget.");
		} else if(amount <=4000){
			console.log("You have resources left");
		}
	} else {
		console.log("Invalid Input.")
	}

};

budgetChecker(6000);